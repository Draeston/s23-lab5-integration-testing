# Lab5 -- Integration testing


### Key: AKfycby1s4fIXUfPk8Oxue9RhFZ2_Tq2m-P2Bz5CapR3eo8KHd_c4qXwVyWfgKxwM-ZsnY8U

### Email: n.morozov@innopolis.university

## Specs

Here is InnoCar Specs:

Budet car price per minute = 26

Luxury car price per minute = 65

Fixed price per km = 26

Allowed deviations in % = 12

Inno discount in % = 8

## BVA

| Argument         | Equivalence classes               |
|------------------|-----------------------------------|
| type             | 'budget', 'luxury', nonsense      |
| plan             | 'minute', 'fixed_price', nonsense |
| distance         | <=0, >0                           |
| planned_distance | <=0, >0                           |
| time             | <=0, >0                           |
| planned_time     | <=0, >0                           |
| inno_discount    | 'yes', 'no', nonsense             |

## Decision table

| №  | type     | plan        | distance | planned_distance | time | planned_time | inno_discount | Expected result | Actual result   | Bug? |
|----|----------|-------------|----------|------------------|------|--------------|---------------|-----------------|-----------------|------|
| 1  | budget   | minute      | 10       | 100              | 10   | 100          | yes           | 239.2           | 239.2           |      |
| 2  | luxury   | minute      | 10       | 100              | 10   | 100          | yes           | 598             | 598             |      |
| 3  | luxury   | fixed_price | 10       | 100              | 10   | 100          | yes           | Invalid Request | Invalid Request |      |
| 4  | nonsense | minute      | 10       | 100              | 10   | 100          | yes           | Invalid Request | Invalid Request |      |
| 5  | budget   | nonsense    | 10       | 100              | 10   | 100          | yes           | Invalid Request | Invalid Request |      |
| 6  | budget   | fixed_price | 10       | 100              | 10   | 100          | yes           | 239.2           | 153.3           | X    |
| 7  | budget   | fixed_price | 10       | 10               | 10   | 100          | yes           | 239,2           | 153.3           | X    |
| 8  | budget   | fixed_price | 0        | 0                | 10   | 100          | yes           | 0               | 153.3           | X    |
| 9  | budget   | fixed_price | 10       | 10               | 0    | 0            | yes           | 239,2           | 0               | X    |
| 10 | budget   | fixed_price | 10       | 10               | 10   | 100          | no            | 260             | 166,6           | X    |
| 11 | budget   | minute      | 0        | 0                | 10   | 100          | no            | 260             | 260             |      |
| 12 | budget   | minute      | 0        | 0                | 10   | 100          | yes           | 239.2           | 239.2           |      |
| 13 | budget   | minute      | 0        | 0                | 0    | 100          | no            | 0               | 0               |      |
| 14 | budget   | minute      | 0        | 0                | -1   | 100          | no            | Invalid request | Invalid request |      |
| 15 | budget   | minute      | 0        | 0                | 0    | 100          | yes           | 0               | 0               |      |
| 16 | budget   | minute      | 0        | 0                | 0    | 0            | no            | 0               | 0               |      |
| 17 | budget   | minute      | 0        | 0                | 0    | 0            | yes           | 0               | 0               |      |
| 18 | luxury   | minute      | 10       | 100              | 10   | 100          | no            | 650             | 650             |      |
| 19 | luxury   | minute      | 0        | 0                | 10   | 100          | yes           | 598             | 598             |      |
| 20 | luxury   | minute      | 0        | 0                | 10   | 100          | no            | 650             | 650             |      |
| 21 | luxury   | minute      | 0        | 0                | 0    | 100          | yes           | 0               | 0               |      |
| 22 | luxury   | minute      | 0        | 0                | -1   | 100          | yes           | Invalid request | Invalid request |      |
| 23 | budget   | fixed_price | -1       | -1               | 10   | 100          | yes           | Invalid request | Invalid request |      |

## Bugs
1. Computational error (type=budget; plan=fixed_price; distance, planned_distance > 0 and in deviation range): wrong result
2. Computational error (type=budget; plan=fixed_price; distance, planned_distance > 0 and not in deviation range): wrong result
3. Computational error (type=budget; plan=fixed_price; distance, planned_distance = 0 and in deviation range): wrong result